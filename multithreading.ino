#define LED1 7
#define LED2 8

// Functions

void turnOnLed1();
void turnOffLed1();
void turnOnLed2();
void turnOffLed2();


long runtime = 0;

// Task definition
typedef struct Task{
  long interval;
  long startAt = 0;
  long differt;
  long stopAt;
  void * (*run)();

  // constructor
  Task(long i, long d, void * (*r)()) {
    interval = i;
    differt = d;
    run = r;
  };

  // begin task
  start() {
    startAt = millis();
    run();
    stopAt = millis();
  }
};


// Task stack
Task tasks[4] = {
  {.interval = 500, .differt = 0, .run = turnOnLed1},
  {.interval = 500, .differt = 250, .run = turnOffLed1},
  {.interval = 4000, .differt = 0, .run = turnOnLed2},
  {.interval = 4000, .differt = 2000, .run = turnOffLed2}
};
size_t n = sizeof(tasks)/sizeof(tasks[0]);

void setup() {
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  int time = millis();
  Task *current;
  //check each task by time
  for(int i = 0;i < n; i++) {
    current = &tasks[i];
    if((time + current->differt) % current->interval == 0) current->start();
    if(time - current->startAt > current->interval) current->start(); //security if loop is too long (> 1ms)
    
  }
  runtime++;
  
}

void turnOnLed1() {
  digitalWrite(LED1, HIGH);
}


void turnOffLed1() {
  digitalWrite(LED1, LOW);
}


void turnOnLed2() {
  digitalWrite(LED2, HIGH);
}


void turnOffLed2() {
  digitalWrite(LED2, LOW);
}
